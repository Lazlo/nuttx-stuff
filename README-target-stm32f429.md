# Target STM32F429

## Build

```
cd nuttx
make distclean
./tools/configure.sh stm32f429i-disco/nsh
make menuconfig
make
```

## Flash Firmware

```
openocd -f interface/stlink-v2.cfg -f target/stm32f4x.cfg -c "program nuttx.hex reset exit"
```
