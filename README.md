# NuttX Stuff

Files related to the NuttX operating system.

## Dependencies

```
apt-get -q install -y build-essential git texinfo libgmp-dev libmpfr-dev libmpc-dev \
libncurses5-dev bison flex gettext gperf libtool autoconf autoconf-archive pkg-config \
libftdi-dev libusb-1.0-0-dev zlib1g zlib1g-dev python-yaml
```

## Sources

```
mkdir nuttx-project
cd nuttx-project
git clone https://bitbucket.org/nuttx/tools.git
git clone https://bitbucket.org/nuttx/nuttx.git
git clone https://bitbucket.org/nuttx/apps.git
git clone https://bitbucket.org/nuttx/buildroot.git
```

## Build Kconfig

```
cd tools/kconfig-frontends
sed -i -E "s/(am__api_version)='1\.15'/\1='1.16'/" configure
./configure --prefix=/usr --enable-mconf
make
sudo make install
cd -
```

## Build Toolchain

```
cd buildroot
cp configs/cortexm4f-eabi-defconfig-7.3.0 .config
make
export PATH=$(pwd)/build_arm_hf/staging_dir/bin:$PATH
cd -
```
