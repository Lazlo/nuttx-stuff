# Create A New Board Config 

Sometimes you will run into situations, where there will be no existing board configuration for the target you want to build NuttX for.
For these situations, this document will describe how to create one.

## Create Board Config Directory Skeleton

For convenience, set the board name as a shell environment variable.

 * create a directory for the board configuration and within it
   * a ```include```  directory (FIXME why?)
   * a ```scripts```  directory
   * a ```src``` directory
   * a ```nsh``` directory (as we will start with NSH as our primary application for this board)

```
cd nuttx
board_name=lazlo-testboard
mkdir -p configs/$board_name/{include, scripts, src, nsh}
```

### Populate Board Config Directory

 * create a empty ```Kconfig``` for board config inside the board config directory
 * find a board configuration of a board that maches the new board as best as possible
   * copy ```include/board.h``` from the similar board to the ```include```directory of the new board
     * adjust the ```#ifndef __CONFIG_.*_INCLUDE_BOARD_H``` macro
     * adjust crystal and clock settings
     * check the ```board.h``` for things that need to be adjusted
   * copy the following files from the ```scripts``` directory of a similar board to the ```scripts``` directory of the new board
     * copy ```Make.defs```
     * copy ```ld.script```
   * copy ```src/Makefile``` and some .c file stub to ```src``` directory of the new board
     * copy ```src/<arch>_boot.c``` and adjust it
     * copy ```src/<arch>_bringup.c```
     * copy ```src/<arch>_ostest.c```
     * copy ```src/<arch>_appinit.c```
     * copy ```src/<arch>_idle.c```
     * copy ```src/<arch>_userleds.c```
     * copy ```src/<boardname>.h```
   * copy ```nsh/defconfig``` from the similar board to the application config directory of new board
     * adjust the ```defconfig``` just copied
     * set ```CONFIG_ARCH_BOARD="stm32f427i"``` to reflect the path to the new board config directory
     * set ```CONFIG_ARCH_BOARD_STM32F427I=y``` (this is the macro to be defined and selected in ```configs/Kconfig``` - see below)
     * adjust ```CONFIG_ARCH_CHIP_.*=y``` to match with the target processor

```
simlilar_board=stm32f429i-disco
touch configs/$board_name/Kconfig
cp configs/$similar_board/scripts/Make.defs configs/$board_name/scripts
cp configs/$similar_board/nsh/defconfig configs/$board_name/nsh
```

## Integrate New Board Config

Now, lets update ```configs/Kconfig``` with the following modifications:

 * add a ```config ARCH_BOARD_STM32F427I``` entry to allow selecting the board config

```
config ARCH_BOARD_STM32F427I
	bool "STM32F427I board"
	depends on ARCH_CHIP_STM32F427I
	---help---
		FIXME Provide some description about the new board ...
```

 * add an entry for ```config ARCH_BOARD``` like this

```
default "stm32f427i" if ARCH_BOARD_STM32F427I
```

 * finally, make sure the board specific ```Kconfig``` file gets sourced conditionally by ```configs/Kconfig```

```
if ARCH_BOARD_STM32F427I
source "configs/stm32f427i/Kconfig"
endif
```

## Test New Board Config

 * run ```make distclean``` to remove previously active configuration
 * load the new board configuration
 * run ```make menuconfig``` to check the expected values are selected
 * finally, run ```make``` to build NuttX for the new board

```
make distclean
./tools/configure.sh $board_name/nsh
make menuconfig
```
